#!/bin/sh

RPTREQS=/usr/local/rptreqs
JAVALIB=/usr/local/java/lib
#
# Set the wasp home directory
WASP_HOME=/opt/wasp
WASP_LIB=$WASP_HOME/lib

#
# Log4J
CLASS_PATH=$CLASS_PATH:$JAVALIB/log4j-1.2.9.jar

#
# Emery Libraries
CLASS_PATH=$CLASS_PATH:$JAVALIB/oagis-2.4.7.jar
CLASS_PATH=$CLASS_PATH:$JAVALIB/emutils-2.8.7.jar

#
# Wasp webservice client libraries
CLASS_PATH=$CLASS_PATH:$WASP_HOME
CLASS_PATH=$CLASS_PATH:$WASP_LIB/wasp.jar
CLASS_PATH=$CLASS_PATH:$WASP_LIB/xercesImpl.jar

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS

export CLASS_PATH
echo "running Item Velocity for Emery"
java -cp $CLASS_PATH:com.emerywaterhouse.data.ItemVelocity.class -server com.emerywaterhouse.data.ItemVelocity

echo "running Item Velocity for Portland"
java -cp $CLASS_PATH:com.emerywaterhouse.data.ItemVelocity.class -server com.emerywaterhouse.data.ItemVelocity 01

echo "running Item Velocity for Pittston"
java -cp $CLASS_PATH:com.emerywaterhouse.data.ItemVelocity.class -server com.emerywaterhouse.data.ItemVelocity 04
