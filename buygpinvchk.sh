#!/bin/sh

cd /usr/local/rptreqs

now="$(date)"
printf "Run date: %s\n" "$now"

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib/*

#
# Set the axis2 lib  directory
AXIS2_LIB=/opt/axis2/lib/*

#
# local dir
CLASS_PATH=$LIB:$AXIS2_LIB:$RPTREQS
export CLASS_PATH

echo starting buy group invoice check
java -cp $CLASS_PATH:com.emerywaterhouse.data.BuyGpInvChk.class -server com.emerywaterhouse.data.BuyGpIncChk production /usr/local/rptreqs/conf/buygpinvchk.conf
echo finished buy group invoice check