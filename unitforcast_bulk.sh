#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib

#
# Set the wasp home directory
WASP_HOME=/opt/wasp
WASP_LIB=$WASP_HOME/lib

#
# Emery Libraries
CLASS_PATH=$CLASS_PATH:$LIB/oagis-2.7.0.jar
CLASS_PATH=$CLASS_PATH:$LIB/emutils-3.1.0.jar
CLASS_PATH=$CLASS_PATH:$LIB/rptreq-1.0.1.jar

#
# Logging libs
CLASS_PATH=$CLASS_PATH:$LIB/log4j-1.2.9.jar

#
# Wasp webservice client libraries
CLASS_PATH=$CLASS_PATH:$WASP_HOME
CLASS_PATH=$CLASS_PATH:$WASP_LIB/wasp.jar
CLASS_PATH=$CLASS_PATH:$WASP_LIB/xercesImpl.jar

#
# Poi libs for reading spreadsheets in bulk loads
CLASS_PATH=$CLASS_PATH:$LIB/poi-3.2-FINAL-20081019.jar
CLASS_PATH=$CLASS_PATH:$LIB/poi-contrib-3.2-FINAL-20081019.jar

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS
export CLASS_PATH

echo starting report run

java -cp $CLASS_PATH:com.emerywaterhouse.bulk.UnitForcast.class -server com.emerywaterhouse.bulk.UnitForcast test "/home/jfisher/Vendor Invitation List.xls"

echo finished report run
