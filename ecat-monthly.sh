#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib/*

#
# axis web service libs
AXIS2=/opt/axis2/lib/*

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS:$LIB:$AXIS2

export CLASS_PATH
echo "running activant ecatalog monthly export"
java -cp $CLASS_PATH:com.emerywaterhouse.data.ECatalogExp.class -server com.emerywaterhouse.data.ECatalogExp production dept ken.shawver@activant.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.ECatalogExp.class -server com.emerywaterhouse.data.ECatalogExp production class ken.shawver@activant.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.ECatalogExp.class -server com.emerywaterhouse.data.ECatalogExp production flc ken.shawver@activant.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.ECatalogExp.class -server com.emerywaterhouse.data.ECatalogExp production vnd ken.shawver@activant.com

