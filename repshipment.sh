#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib

#
# Set the wasp home directory
WASP_HOME=/opt/wasp
WASP_LIB=$WASP_HOME/lib

#
# Emery Libraries
CLASS_PATH=$CLASS_PATH:$LIB/oagis-2.7.0.jar
CLASS_PATH=$CLASS_PATH:$LIB/emutils-3.1.0.jar
CLASS_PATH=$CLASS_PATH:$LIB/rptreq-1.0.1.jar

#
# Logging libs
CLASS_PATH=$CLASS_PATH:$LIB/log4j-1.2.9.jar

#
# Wasp webservice client libraries
CLASS_PATH=$CLASS_PATH:$WASP_HOME
CLASS_PATH=$CLASS_PATH:$WASP_LIB/wasp.jar
CLASS_PATH=$CLASS_PATH:$WASP_LIB/xercesImpl.jar

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS
export CLASS_PATH

echo starting report run

java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Sheera Solomont" ssolomont@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Ahron Solomont" asolomont@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Kevin Koster" kkoster@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Larry Flanagan" lflanagan@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Mike Zimbile" mzimbile@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Carl Sundberg" csundberg@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Bernando Taveras" btaveras@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Sumner Kessler" skessler@emeryonline.com,lgrenier@emeryonline.com,thartford@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Bobby Brown" bbrown@emeryonline.com,jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Alex Hubert" jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Bob Kinchla" bkinchla@emeryonline.com,jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Jim Welch" jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Levi Robbins" lrobbins@emeryonline.com,jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Peter Talbot" ptalbot@emeryonline.com,jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Ray Manyak" rmanyak@emeryonline.com,jhanegan@emeryonline.com
java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Steve Goodspeed" jhanegan@emeryonline.com

# Used for testing, don't remove and only uncomment during a test
#java -cp $CLASS_PATH:com.emerywaterhouse.data.RepShipments.class -server com.emerywaterhouse.data.RepShipments production "Steve Goodspeed" jfisher@emeryonline.com

echo finished report run
