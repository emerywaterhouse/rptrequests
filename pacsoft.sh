#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib

#
# Set the wasp home directory
WASP_HOME=/usr/local/wasp654
WASP_LIB=$WASP_HOME/lib

#
# Emery Libraries
CLASS_PATH=$CLASS_PATH:$LIB/oagis-2.8.01.jar
CLASS_PATH=$CLASS_PATH:$LIB/emutils-3.2.0.jar
CLASS_PATH=$CLASS_PATH:$LIB/rptreq-1.0.2.jar
CLASS_PATH=$CLASS_PATH:$LIB/wsbeans-2.2.1.jar

#
# Logging libs
CLASS_PATH=$CLASS_PATH:$LIB/log4j-1.2.9.jar

#
# Wasp webservice client libraries
CLASS_PATH=$CLASS_PATH:$WASP_HOME
CLASS_PATH=$CLASS_PATH:$WASP_LIB/wasp.jar
CLASS_PATH=$CLASS_PATH:$WASP_LIB/xercesImpl.jar

#
# DB libs
CLASS_PATH=$CLASS_PATH:$LIB/ojdbc14.jar

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS
export CLASS_PATH

echo starting catalog export for Pacsoft

java -cp $CLASS_PATH:com.emerywaterhouse.data.CatalogExport.class -server com.emerywaterhouse.data.CatalogExport production 299596

echo finished catalog export for Pacsoft
