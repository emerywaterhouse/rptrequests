#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib/*

#
# Set the axis2 libs
AXIS2=/opt/axis2/lib/*

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS:$LIB:$AXIS2
export CLASS_PATH

echo "running emery link monthly export"
java -cp $CLASS_PATH:com.emerywaterhouse.data.DbExport.class -server com.emerywaterhouse.data.DbExport production dataprocessing@emeryonline.com