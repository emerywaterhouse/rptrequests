#!/bin/sh

cd /usr/local/rptreq

now="$(date)"
printf "Run date: %s\n" "$now"

RPTREQS=/usr/local/rptreq
LIB=$RPTREQS/lib/*

#
# Set the axis2 lib  directory
AXIS2_LIB=/opt/axis2/lib/*

#
# local dir
CLASS_PATH=$LIB:$AXIS2_LIB:$RPTREQS
export CLASS_PATH

echo "running Edi invoices not confirmed report"
java -cp $CLASS_PATH:com.emerywaterhouse.data.EdiInvNotConfExp.class -server com.emerywaterhouse.data.EdiInvNotConfExp production programming@emeryonline.com
echo "finished running Edi invoices not confirmed report"
