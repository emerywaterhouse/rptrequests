#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib/*

#
# Set the axis2 libs
AXIS2=/opt/axis2/lib/*
SS_PATH:$WASP_LIB/xercesImpl.jar

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS:$LIB:$AXIS2

export CLASS_PATH

echo "running kreg item movement export"
java -cp $CLASS_PATH:com.emerywaterhouse.data.KregDataExp.class -server com.emerywaterhouse.data.KregDataExp production jfisher@emeryonline.com,commissions@kregtool.com
