/**
 * File: CatalogExport.java
 * Description: Generates the report request to create a customer specific catalog file.
 *
 * @author Jeff Fisher
 *
 * Create Date: 11/23/2011
 * Last Update: $Id: CatalogExport.java,v 1.7 2016/10/07 14:04:12 tli Exp $
 *
 * History:
 *    $Log: CatalogExport.java,v $
 *    Revision 1.7  2016/10/07 14:04:12  tli
 *    Converted b2b_user tables to postgresql
 *
 *    Revision 1.6  2015/10/16 18:56:52  jfisher
 *    Changes for overwriting existing files.
 *
 *    Revision 1.5  2014/01/02 19:43:54  jfisher
 *    Multi warehouse changes
 *
 *    Revision 1.4  2013/07/16 12:09:05  jfisher
 *    Switched to GROK
 *
 *    Revision 1.3  2012/10/02 11:04:28  jfisher
 *    Margin master mods
 *
 *    Revision 1.2  2011/12/13 11:51:12  jfisher
 *    Added the POS vendor attribute.
 *
 *    Revision 1.1  2011/11/29 07:52:58  jfisher
 *    Added the ftp element to the template
 *
 */
package com.emerywaterhouse.data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import com.emerywaterhouse.b2bsvc.TradingPartnerInfo;
import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;
import com.emerywaterhouse.utils.DbUtils;
public class CatalogExport extends RptReq
{
   protected String m_Accts;        /** The comma separated list of customer account numbers */
   private StringBuffer m_Recips;
   private String m_PosVnd;         /** The POS vendor if there was one */
   private Connection m_EdbConn;       /** internal DB connection used for setup information */
   private boolean m_OverWrite;     /** Determines if the catalog file should be overwritten */
   /**
    * Default constructor.
    */
   public CatalogExport()
   {
      super();
      //
      // Add the default data processing email address so the IT department gets notified.
      m_Recips = new StringBuffer();
      m_Recips.append(String.format(RptReq.recipTag, "dp", "dataprocessing@emeryonline.com"));
      m_PosVnd = "";
      m_OverWrite = false;
   }
   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;
      String custId = args[0];
      String uid = args[1];
      String pwd = args[2];
      String url = args[3];
      try {
         params.append(String.format(RptReq.paramTag, "cust", "String", custId));
         params.append(String.format(RptReq.paramTag, "posvnd", "String", m_PosVnd));
         params.append(String.format(RptReq.paramTag, "dc", "String", getDc(custId)));
         params.append(String.format(RptReq.paramTag, "overwrite", "String", m_OverWrite ? "true":"false"));
         params.append(String.format(RptReq.paramTag, "datafmt", "String", "xml"));
         params.append(String.format(RptReq.paramTag, "datasrc", "String", ""));
         params.append(String.format(RptReq.paramTag, "srcid", "String", ""));
         params.append(String.format(RptReq.paramTag, "escapeformat", "String", "xml"));
         if ( m_PosVnd != null && m_PosVnd.equalsIgnoreCase("marginmaster") )
            params.append(String.format(RptReq.paramTag, "includeretails", "String", "true"));
         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "yes", "no",
               String.format("%s %s Catalog Export", m_PosVnd, custId),
               "com.emerywaterhouse.rpt.export.Catalog",
               params.toString(),
               m_Recips.toString(),
               String.format(RptReq.ftpTag, url, uid, pwd)
         );
      }
      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }
      finally {
         params = null;
         uid = null;
         pwd = null;
         url = null;
      }
      return xml;
   }
   /**
    * Close the global db connection.
    */
   public void closeDbConn()
   {
      DbUtils.closeDbConn(m_EdbConn, null, null);
   }
   /**
    * Parses a comma separated list of customer numbers and returns the account and transport information
    * for that account.
    *
    * @return An ArrayList reference containing the transport information for each customer account number.
    * @throws Exception
    */
   public ArrayList<TradingPartnerInfo> getAcctInfo() throws Exception
   {
      String[] list = m_Accts.split("[,]");
      ArrayList<TradingPartnerInfo> acctInfo = new ArrayList<TradingPartnerInfo>();
      TradingPartnerInfo tmp = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      StringBuffer sql = new StringBuffer();
      sql.append("select physical_addr, user_id, password, b2b_transport.name ");
      sql.append("from b2b_server_map ");
      sql.append("join b2b_transport on b2b_transport.transport_id = b2b_server_map.transport_id ");
      sql.append("join b2b_user_map on b2b_user_map.map_id = b2b_server_map.map_id and outbound = 1 ");
      sql.append("join b2b_user on b2b_user.b2b_user_id = b2b_user_map.b2b_user_id and customer_id = ? ");
      sql.append("join b2b_access on b2b_access.b2b_user_id = b2b_user_map.b2b_user_id");
      stmt = m_EdbConn.prepareStatement(sql.toString());
      try {
         for ( int i = 0; i < list.length; i++ ) {
            stmt.setString(1, list[i]);
            rs = stmt.executeQuery();
            if ( rs.next() ) {
               tmp = new TradingPartnerInfo();
               tmp.setPartnerId(list[i]);
               tmp.getTransportInfo().uid = rs.getString("user_id");
               tmp.getTransportInfo().pwd = rs.getString("password");
               tmp.getTransportInfo().uri = rs.getString("physical_addr");
               acctInfo.add(tmp);
            }
            else
               log.error(String.format("[catalog export] unable to get transport information for %s", list[i]));
            DbUtils.closeDbConn(null, null, rs);
         }
      }
      finally {
         DbUtils.closeDbConn(null, stmt, rs);
         stmt = null;
         rs = null;
      }
      return acctInfo;
   }
   /**
    * Gets the warehouse ID for each customer.
    * @param custId
    * @return The internal warehouse id.
    * @throws SQLException
    */
   private String getDc(String custId) throws SQLException
   {
      String dc = "1";
      PreparedStatement stmt = null;
      ResultSet rs = null;
      StringBuffer sql = new StringBuffer();
      if ( m_EdbConn != null ) {
         sql.append("select warehouse_id ");
         sql.append("from cust_warehouse ");
         sql.append("where customer_id = ?");
         try {
            stmt = m_EdbConn.prepareStatement(sql.toString());
            stmt.setString(1, custId);
            rs = stmt.executeQuery();
            if ( rs.next() )
               dc = rs.getString(1);
         }
         finally {
            DbUtils.closeDbConn(null, stmt, rs);
            rs = null;
            stmt = null;
            sql = null;
         }
      }
      return dc;
   }
   /**
    * @param args
    */
   public static void main(String[] args)
   {
      CatalogExport export = new CatalogExport();
      ArrayList<TradingPartnerInfo> acctInfo = null;
      Iterator<TradingPartnerInfo> iter = null;
      String rptXml = null;
      String msg = null;
      TradingPartnerInfo tpi = null;
      String[] tmp = new String[4];
      try {
         log.info("[catalog export] starting catalog request");
         export.parseCommandLine(args);
         export.openDbConn();
         acctInfo = export.getAcctInfo();
         iter = acctInfo.iterator();
         while ( iter.hasNext() ) {
            tpi = iter.next();
            tmp[0] = tpi.getPartnerId();
            tmp[1] = tpi.getTransportInfo().uid;
            tmp[2] = tpi.getTransportInfo().pwd;
            tmp[3] = tpi.getTransportInfo().uri;
            try {
               rptXml = export.buildRptRequest(tmp);
               if ( rptXml != null && rptXml.length() > 0 ) {
                  try {
                     log.info(String.format("[catalog export] sending catalog request for %s", tpi.getTransportInfo().uid));
                     export.sendRpt(new Object[] {rptXml});
                     log.info(String.format("[catalog export] catalog request for %s sent", tpi.getTransportInfo().uid));
                  }
                  catch( Exception ex ) {
                     log.error("[catalog export] ", ex);
                  }
               }
               else
                  msg = "[catalog export] error creating the report request bod";
            }
            catch ( Exception ex ) {
               log.error("[catalog export]", ex);
            }
            finally {
               if ( export.getMsg().length() > 0 )
                  log.error(export.getMsg());
               if ( msg != null && msg.length() > 0 )
                  log.error(msg);
               msg = null;
               export.clearMsg();
            }
         }
      }
      catch ( Exception ex ) {
         ex.printStackTrace(System.err);
      }
      finally {
         export.closeDbConn();
         export = null;
         tmp = null;
         rptXml = null;
         iter = null;
         acctInfo = null;
      }
   }
   /**
    * Opens a database connection.
    * @throws Exception
    */
   public void openDbConn() throws Exception
   {
      String url = "";
      String pwd = "";
      if ( m_Env == Environment.Test ) {
         url = "jdbc:edb://10.128.0.127:5444/emery_jensen";
         pwd = "mugwump";
      }
      else {
         url = "jdbc:edb://10.128.0.11:5444/emery_jensen";
         pwd = "boxer";
      }
      DriverManager.registerDriver(new com.edb.Driver());
      m_EdbConn = DriverManager.getConnection (url, "ejd", pwd);
   }
   /**
    * Parses the command line. The base class gets the environment.
    *
    * @param args The command line parameter list.
    * @throws Exception
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      super.parseCommandLine(args);
      //
      // The first argument is supposed to be the environment, second the account list, third the POS vendor.
      if ( pcount >= 2 && pcount <= 4 ) {
         m_Accts = args[1];
        if ( pcount >= 3 )
            m_PosVnd = args[2];
        if ( pcount == 4 )
            m_OverWrite = Boolean.parseBoolean(args[3]);
      }
      else
         throw new Exception("incorrect number of parameters; should be env, customer account list");
   }
}
