/**
 * File: ECatalogExp.java
 * Description: Cron app for sending the report requests that generate the data export files for the
 *    Activant eCatalog.
 *
 * @author Jeff Fisher
 *
 * Create Date: 03/24/2010
 * Last Update: $Id: ECatalogExp.java,v 1.2 2012/10/02 11:03:55 jfisher Exp $
 *
 * History: $Log: ECatalogExp.java,v $
 * History: Revision 1.2  2012/10/02 11:03:55  jfisher
 * History: Reports now going to the epicor ftp server
 * History:
 * History: Revision 1.1  2012/09/20 18:18:21  jfisher
 * History: Moved to the report request project from activant data
 * History:
 * History: Revision 1.2  2012/05/08 09:33:54  jfisher
 * History: updated web service url
 * History:
 * History: Revision 1.1  2010/03/28 22:53:17  jfisher
 * History: Initial add
 * History:
 */
package com.emerywaterhouse.data;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;

public class ECatalogExp extends RptReq
{
   private StringBuffer m_Recips;
   private String m_Type;

   /**
    *
    */
   public ECatalogExp()
   {
      m_Recips = new StringBuffer();
   }

   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
         params.append(String.format(RptReq.paramTag, "type", "String", m_Type));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "yes", "no",
               "Activant Export",
               "com.emerywaterhouse.rpt.export.ActivantECatalog",
               params.toString(),
               m_Recips.toString(),
               String.format(RptReq.ftpTag, "lynx.ccitriad.com:Emery", "JeffFisher", "Jeff229Emery")
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String recips = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 3: {
            m_Type = args[1];
            recips = args[2];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be env, report type and emailaddr");
         }
      }

      //
      // email addresses are a comma separated list.
      if ( recips != null && recips.length() > 0 ) {
         String[] result = recips.split("[,]");

         for ( int i = 0; i < result.length; i++ )
            m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
      }
   }

   /**
    * Launches the report request.
    *
    * @param args The command line parameters.
    * @throws Exception
    */
   public static void main(String[] args)
   {
      ECatalogExp rpt = new ECatalogExp();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[activant export] sending report request for %s", rpt.m_Type));
            rpt.sendRpt(new Object[] {rptXml});
            log.info(String.format("[activant export] report request for %s sent", rpt.m_Type));
         }

         catch( Exception ex ) {
            log.error("[activant export] ", ex);
         }
      }
      else
         msg = "[activant export] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }
}
