package com.emerywaterhouse.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;


public class EdiInvNotConfExp extends RptReq
{
   private static boolean DEBUG = false;

   private StringBuffer m_Recips;
   private Boolean m_AllItems;
   private String m_CustId;
   private Boolean m_OverWrite;
   private String m_Url;
   private String m_Uid;
   private String m_Pwd;
   private Boolean m_Zip;

   /**
    *
    */
   public EdiInvNotConfExp()
   {
      m_Recips = new StringBuffer();
   }

   /**
    * @see com.emerywaterhouse.template.RptReq#buildRptRequest(java.lang.String[])
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
//         params.append(String.format(RptReq.paramTag, "cust", "String", m_CustId));
  //       params.append(String.format(RptReq.paramTag, "ov48erwrite", "String", m_OverWrite.toString()));
    //     params.append(String.format(RptReq.paramTag, "allitems", "String", m_AllItems.toString()));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "The report for not-confirmed-EDI-invoices",
               "com.emerywaterhouse.rpt.text.EdiInvNotConf",
               params.toString(),
               m_Recips.toString(),
               ""
         );

         if ( !DEBUG ) {
            //
            // Crude, but effective when you don't want to get acks or send emails.
            xml = xml.replaceAll("Always", "Never");
         }
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
         log.error("[EdiInvNotConfExp]", ex);
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      EdiInvNotConfExp rpt = new EdiInvNotConfExp();
      String rptXml = null;
      String msg = null;

      try {
         rptXml = rpt.buildRptRequest(args);

         if ( rptXml != null && rptXml.length() > 0 ) {
            try {
               log.info(String.format("[EdiInvNotConfExp] sending Edi Inv not confirmed report request"));
               rpt.sendRpt(new Object[] {rptXml});
               log.info(String.format("[EdiInvNotConfExp] Edi Inv not confirmed report request sent"));
            }

            catch( Exception ex ) {
               log.error("[EdiInvNotConfExp]", ex);
            }
         }
         else
            msg = "[EdiInvNotConfExp] error creating the report request bod";

         if ( rpt.getMsg().length() > 0 )
            log.error(rpt.getMsg());

         if ( msg != null && msg.length() > 0 )
            log.error(msg);
      }

      catch ( Exception ex ) {
         log.error("[EdiInvNotConfExp]", ex);
      }
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String recips = null;
      String paramFile = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 2: {
            paramFile = args[1];
            break;
         }

         case 3: {
            paramFile = args[1];
            recips = args[2];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be env paramFile [recips]");
         }
      }

      //parseParamFile(paramFile);

      //
      // email addresses are a comma separated list.
      if ( recips != null && recips.length() > 0 ) {
         String[] result = recips.split("[,]");

         for ( int i = 0; i < result.length; i++ )
            m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
      }
   }

   /**
    * Parse the parameter file which will have information such as customer id, dc, etc.
    * @param fileName
    * @throws IOException
    */
   public void parseParamFile(String fileName) throws IOException
   {
      FileInputStream stream = new FileInputStream(new File(fileName));
      String params = "";

      try {
        FileChannel fc = stream.getChannel();
        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

        params = Charset.defaultCharset().decode(bb).toString();
      }

      finally {
        stream.close();
      }

      //
      // parameters are a comma separated list.
      if ( params != null && params.length() > 0 ) {
         String[] result = params.split("[,]");

         m_CustId = result[0];
         m_OverWrite = Boolean.parseBoolean(result[1]);
         m_AllItems = Boolean.parseBoolean(result[2]);
         m_Zip = Boolean.parseBoolean(result[3]);
         m_Url = result[4];
         m_Uid = result[5];
         m_Pwd = result[6];
      }
   }
}
