/**
 * File: ClosedRcvrs.java
 * Description: Launches the closed receiver Excel spreadsheet report from cron
 *
 * @author Jeff Fisher
 *
 * Create Date: 08/10/2011
 * Last Update: $Id: ClosedRcvrs.java,v 1.2 2011/11/29 07:51:52 jfisher Exp $
 * $Revision: 1.2 $
 *
 * History:
 *    $Log: ClosedRcvrs.java,v $
 *    Revision 1.2  2011/11/29 07:51:52  jfisher
 *    Added the ftp element to the template
 *
 *    Revision 1.1  2011/08/12 02:08:49  jfisher
 *    initial add
 *
 */
package com.emerywaterhouse.data;

import java.util.Calendar;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;


public class ClosedRcvrs extends RptReq
{
   private StringBuffer m_Recips;
   private String m_Date;

   public ClosedRcvrs()
   {
      super();
      m_Recips = new StringBuffer();
   }

   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_Date = null;
      m_Recips = null;
   }

   /**
    * @see com.emerywaterhouse.template.RptReq#buildRptRequest(java.lang.String[])
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);

         if ( m_Date != null && m_Date.length() > 0 )
            params.append(String.format(RptReq.paramTag, "RptDate", "String", m_Date));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "Closed Receivers",
               "com.emerywaterhouse.rpt.spreadsheet.ClosedReceivers",
               params.toString(),
               m_Recips.toString(),
               ""
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String recips = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 1: break;

         case 2: {
            recips = args[1];
            break;
         }

         case 3: {
            recips = args[1];
            m_Date = args[2];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be env, recipients or env, recipients, date");
         }
      }

      //
      // email addresses are a comma separated list.
      if ( recips != null && recips.length() > 0 ) {
         String[] result = recips.split("[,]");

         for ( int i = 0; i < result.length; i++ )
            m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
      }

      //
      // If no date was given, then go back one day from the current date.
      if ( m_Date == null || m_Date.length() == 0 ) {
         Calendar cal = Calendar.getInstance();
         cal.roll(Calendar.DAY_OF_YEAR, false);

         m_Date = String.format("%td-%tb-%tY", cal, cal, cal);
      }
   }

   /**
    * Launches the report request.
    *
    * @param args The command line parameters.
    * @throws Exception
    */
   public static void main(String[] args)
   {
      ClosedRcvrs rpt = new ClosedRcvrs();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[closedrcvr] sending report request for %s", rpt.m_Date));
            rpt.sendRpt(new Object[] {rptXml});
            log.info(String.format("[closedrcvr] report request for %s sent", rpt.m_Date));
         }

         catch( Exception ex ) {
            log.error("[closedrcvr] ", ex);
         }
      }
      else
         msg = "[closedrcvr] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }

}
