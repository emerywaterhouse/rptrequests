/**
 * File: InvCoverage.java
 * Description: Generates the report request for the Buy Group Invoice Check report.
 * 
 * @author Eric Verge
 * 
 * Create Date: 12/04/2014
 * Last Update: $Id: InvCoverage.java,v 1.1 2014/12/12 22:36:31 everge Exp $
 * 
 * History: $Log: InvCoverage.java,v $
 * History: Revision 1.1  2014/12/12 22:36:31  everge
 * History: Initial commit
 * History:
 *
 */

package com.emerywaterhouse.data;

import java.io.BufferedReader;
import java.io.FileReader;
//import java.util.regex.Pattern;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;

public class InvCoverage extends RptReq {
   
   private StringBuffer m_Recips;      /** List of email recipients */
   
   /**
    * Default constructor.
    */
   public InvCoverage() {
      super();
	   
	  m_Recips = new StringBuffer(); 
   }
   
   /**
    * Builds the report request XML BOD. 
    * 
    * @param args The command line argument array from the main method.
    *
    * @return The BOD XML string.
    */
   @Override
   protected String buildRptRequest(String[] args) {
      String xml = null;
      
      try {
         parseCommandLine(args);
         
         xml = String.format(RptReq.procRptReqBod, 
            OagisUtils.getOagisDateTime(),
            OagisUtils.createBODId(),
            "yes", "yes",
            "Inventory Coverage Report",
            "com.emerywaterhouse.rpt.spreadsheet.InventoryCoverage", "",
            m_Recips.toString(), "");
      }
      
      catch ( Exception ex ) {
          m_Msg.append(ex.toString());
      }
      
      return xml;
   }

   /**
    * Parses the command line. The superclass method gets the first parameter 
    * which should always be the environment. This method gets the second 
    * parameter which should be a path to a file containing the report request
    * parameters. The file is then parsed by the <code>parseParamFile</code> method.
    *
    * @param args The command line parameter list.
    * @throws Exception
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception {
      int pcount = args.length;
      String paramFile = null;

      super.parseCommandLine(args);
      
      switch ( pcount ) {
         case 2: {
            paramFile = args[1];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; must include path to config file");
         }
      }

      parseParamFile(paramFile);    
   }
    
   /**
    * Parses the report request parameter file. In this case the file contains a
    * comma-delimited list of email addresses for the recipients of the report.
    * 
    * @param fileName The path to the file containing the report request params.
    * @throws Exception
    */
   public void parseParamFile(String fileName) throws Exception {
      BufferedReader br = null;
	   String line = "";
      String[] emails = null;
      final String REGEX = "\\s*,[,\\s]*";
      
      try {
         FileReader reader = new FileReader(fileName);
         br = new BufferedReader(reader);
          
         while ( (line = br.readLine()) != null ) {
            emails = line.split(REGEX);
            
            for ( String addr : emails) {
                m_Recips.append(String.format(RptReq.recipTag, "", addr));
            }
         }
      }
      
      finally {
         br.close();
      }
   }
   
   /**
    * Builds the report request and sends it to the report server.
    * 
    * @param args Environment setting and path to param file.
    */
   public static void main(String[] args) {
      InvCoverage rpt = new InvCoverage();
	  String rptXml = null;
	  String msg = null;

	  rptXml = rpt.buildRptRequest(args);

	  if ( rptXml != null && rptXml.length() > 0 ) {
	     try {
	        log.info("[InventoryCoverage] sending report request");
	        rpt.sendRpt(new Object[] {rptXml});
	        log.info("[InventoryCoverage] report request sent");
	     }

	     catch( Exception ex ) {
	        log.error("[InventoryCoverage]", ex);
	     }
	  }
	  else
	     msg = "[InventoryCoverage] error creating the report request bod";

	  if ( rpt.getMsg().length() > 0 )
	     log.error(rpt.getMsg());

	  if ( msg != null && msg.length() > 0 )
	     log.error(msg);
   }
   
}
