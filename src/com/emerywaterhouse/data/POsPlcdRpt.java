/**
 * File: POReport.java
 * Description: Generates the report request to create the PO report.  This will be run by a cron job
 *
 * Note: The report request can include a begin and end date, however if they are not included it will default to a week ago and today, respectively
 *
 * @author Eric Brownewell
 * 
 * Create Date: 08/20/2014
 * Last Update: $Id: POsPlcdRpt.java,v 1.3 2015/07/27 17:33:49 ebrownewell Exp $
 *
 * History:
 * 
 */
package com.emerywaterhouse.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;


public class POsPlcdRpt extends RptReq
{
   private String m_BegDate;
   private String m_EndDate;
   private StringBuffer m_Recips;

   /**
    * Default constructor. 
    */
   public POsPlcdRpt()
   {
      super();
      m_Recips = new StringBuffer();
      //TODO: update recips when that information is provided
      //m_Recips.append(String.format(RptReq.recipTag, "ebrownewell", "ebrownewell@emeryonline.com")); 
      m_Recips.append(String.format(RptReq.recipTag, "Dean Frost", "dfrost@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Fred Arsenault", "FArsenault@emeryonline.com"));
   }

   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_BegDate = null;
      m_EndDate = null;
      m_Recips = null;
   }

   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
         if ( m_BegDate != null && m_BegDate.length() > 0 )
            params.append(String.format(RptReq.paramTag, "begdate", "String", m_BegDate));

         if ( m_EndDate != null && m_EndDate.length() > 0 )
            params.append(String.format(RptReq.paramTag, "enddate", "String", m_EndDate));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "Purchase Orders Placed",
               "com.emerywaterhouse.rpt.spreadsheet.POsPlacedRpt",
               params.toString(),
               m_Recips.toString(),
               ""
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 3: {
            m_BegDate = args[1];
            m_EndDate = args[2];
            break;
         }

         default: {
        	 System.out.println("using default parameters");
        	 String[] defaultDates = setDefaultDates();
        	 m_BegDate = defaultDates[0];
        	 m_EndDate = defaultDates[1];
        	 
         }
      }
   }
   /**
    * internal function to set default dates if we don't receive them on the command line.
    * 
    * @return A string array holding the two default dates, [0]: formatted date string from 7 days ago. [1]: formatted date string from today.
    */
   private static String[] setDefaultDates()
   {
		//set default begin and end dates in case we dont get them.
	   	String[] results = new String[2];
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date weekAgo = getDateInPast(now, 7);
		results[0] = dateFormat.format(weekAgo);
		results[1] = dateFormat.format(now);
		
		return results;
   }
   
   /**
    * Internal utility function to get a date from a supplied number of days ago.
    * @param date The date to use as a reference.
    * @param numDays The number of days to set the date back.
    * 
    * @return a date object that is numDays before the date that was passed in
    */
   private static Date getDateInPast(final Date date, int numDays)
   {
	   Date result = new Date(date.getTime());
	   GregorianCalendar calendar = new GregorianCalendar();
	   calendar.setTime(result);
	   calendar.add(Calendar.DATE,-numDays);
	   result.setTime(calendar.getTime().getTime());
	   
	   return result;
   }

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      POsPlcdRpt rpt = new POsPlcdRpt();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info("[POsPlacedRpt] sending report request ");
            rpt.sendRpt(new Object[] {rptXml});
            log.info("[POsPlacedRpt] report request sent");
         }

         catch( Exception ex ) {
            log.error("[POsPlacedRpt] ", ex);
         }
      }
      else
         msg = "[POsPlacedRpt] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }
}