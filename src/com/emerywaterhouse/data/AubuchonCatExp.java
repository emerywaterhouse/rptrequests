/**
 * 
 */
package com.emerywaterhouse.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.emerywaterhouse.b2bsvc.TradingPartnerInfo;
import com.emerywaterhouse.b2bsvc.Transports;
import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;
import com.emerywaterhouse.utils.DbUtils;

/**
 * @author JFisher
 *
 */
public class AubuchonCatExp  extends RptReq
{
   /**
    * Environment enumeration
    */
   private enum CatalogType {
      txt,
      xls,
      xml
   };
   
   private String m_CustId;            /** The customer account number */
   private String m_RptCust;           /** The customer id used to get the pricing and configure the warehouse to pull items from.*/
   private CatalogType m_CatType;      /** A flag to determine which spruce catalog file to send*/
   private String m_LogicalId;         /** The logical id of the B2B mapping */
   private StringBuffer m_Recips;      /** List of email recipients */
   private TradingPartnerInfo m_Tpi;   /** The trading partner information */
   private boolean m_Zip;              /** Flag to compress the file */
   
   static final String PtldCust = "074594";
   static final String PittCust = "075281";
      
   /**
    * 
    */
   public AubuchonCatExp() 
   {
      m_Recips = new StringBuffer();
      m_Tpi = new TradingPartnerInfo();
      m_CatType = CatalogType.xml;
      m_Zip = true;
   }

   @Override
   protected String buildRptRequest(String[] args) 
   {
      StringBuffer params = new StringBuffer();      
      String xml = null;
      int transport;
      String ftpStr = "";
      String catalogType = null;
      boolean attach = false;
      
      try {
         parseCommandLine(args);
         getPartnerInfo();
         transport = m_Tpi.getTransportInfo().transport;

         //
         // Determine how we are supposed to send the data.  It will be based on the B2B entry.
         // Spruce is X12 which we currently send via AS3/FTP.  If it's SMTP, then we need to add that address
         // to the recipients.
         switch ( transport ) {
            case Transports.as3:
            case Transports.ftp: {
               ftpStr = String.format(RptReq.ftpTag, m_Tpi.getTransportInfo().uri, m_Tpi.getTransportInfo().uid, m_Tpi.getTransportInfo().pwd);
               break;
            }

            case Transports.smtp: {
               m_Recips.append(String.format(RptReq.recipTag, "", m_Tpi.getTransportInfo().uri));
               attach = true;
               break;
            }

            default: {
               ftpStr = String.format(RptReq.ftpTag, m_Tpi.getTransportInfo().uri, m_Tpi.getTransportInfo().uid, m_Tpi.getTransportInfo().pwd);
            }
         }

         switch ( m_CatType ) {
            case txt: {
               catalogType = "com.emerywaterhouse.rpt.export.Aubuchon";   // This is the defacto text file catalog
               params.append(String.format(RptReq.paramTag, "custid", "String", m_CustId));
               params.append(String.format(RptReq.paramTag, "cattype", "String", "text"));
               params.append(String.format(RptReq.paramTag, "acedc", "String", "11"));       
               break;
            }
   
            case xls: {
               catalogType = "com.emerywaterhouse.rpt.export.Aubuchon";  // Spruce spreadsheet
               params.append(String.format(RptReq.paramTag, "custid", "String", m_CustId));
               params.append(String.format(RptReq.paramTag, "cattype", "String", "excel"));
               params.append(String.format(RptReq.paramTag, "acedc", "String", "11"));       
               break;
            }
   
            case xml: {
               catalogType = "com.emerywaterhouse.rpt.export.Aubuchon";  // regular xml catalog
               params.append(String.format(RptReq.paramTag, "cust", "String", m_CustId));    
               params.append(String.format(RptReq.paramTag, "cattype", "String", "xml"));     
               params.append(String.format(RptReq.paramTag, "acedc", "String", "11"));                      
               break;
            }
   
            default: {
               catalogType = "com.emerywaterhouse.rpt.export.Aubuchon";
               params.append(String.format(RptReq.paramTag, "custid", "String", m_CustId));
               params.append(String.format(RptReq.paramTag, "cattype", "String", "text"));
               params.append(String.format(RptReq.paramTag, "acedc", "String", "11"));       
            }
         };
         
         xml = String.format(RptReq.procRptReqBod,
            OagisUtils.getOagisDateTime(),
            OagisUtils.createBODId(),
            m_Zip ? "yes" : "no",
            attach ? "yes" : "no",
            m_CustId + " Aubuchon Catalog Export",
            catalogType,
            params.toString(),
            m_Recips.toString(),
            ftpStr
         );
      }
   
      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }
   
      finally {
         params = null;
      }

      return xml;
   }
   
   
   /**
    * Gets the trading partner information from the database.
    *
    * @throws Exception
    */
   public void getPartnerInfo() throws Exception
   {
      Connection conn = null;
      PreparedStatement stmt = null;
      ResultSet rs = null;
      StringBuffer sql = new StringBuffer();
      String transName = "";

      DriverManager.registerDriver(new com.edb.Driver());
      conn = DriverManager.getConnection ("jdbc:edb://10.128.0.11/emery_jensen", "ejd", "boxer");
      sql.append("select physical_addr, user_id, password, b2b_transport.name ");
      sql.append("from b2b_server_map ");
      sql.append("join b2b_transport on b2b_transport.transport_id = b2b_server_map.transport_id ");
      sql.append("join b2b_user_map on b2b_user_map.map_id = b2b_server_map.map_id and outbound = 1 ");
      sql.append("join b2b_user on b2b_user.b2b_user_id = b2b_user_map.b2b_user_id and customer_id = ? and logical_id = ? ");
      
      stmt = conn.prepareStatement(sql.toString());
      stmt.setString(1, m_CustId);
      stmt.setString(2, m_LogicalId);
      rs = stmt.executeQuery();

      if ( rs.next() ) {
         m_Tpi.setPartnerId(m_CustId);
         m_Tpi.getTransportInfo().uid = rs.getString("user_id");
         m_Tpi.getTransportInfo().pwd = rs.getString("password");
         m_Tpi.getTransportInfo().uri = rs.getString("physical_addr");

         transName = rs.getString("name");

         //
         // Right now we only support sending a catalog with these three methods to spruce.
         if ( transName.equalsIgnoreCase("ftp") )
            m_Tpi.getTransportInfo().transport = Transports.ftp;
         else {
            if ( transName.equalsIgnoreCase("smtp") )
               m_Tpi.getTransportInfo().transport = Transports.smtp;
            else {
               if ( transName.equalsIgnoreCase("as3") )
                  m_Tpi.getTransportInfo().transport = Transports.as3;
               else
                  m_Tpi.getTransportInfo().transport = Transports.useDefault;
            }
         }
      }
      else
         log.error(String.format("[AubuchonCatExp] unable to get transport information for %s", m_CustId));

      DbUtils.closeDbConn(conn, stmt, rs);
   }
   
   /**
    * Parses the command line. The base class gets the environment.
    *
    * @param args The command line parameter list.
    * @throws Exception
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String paramFile = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 2: {
            paramFile = args[1];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be env and paramFile");
         }
      }

      parseParamFile(paramFile);
   }
   
   /**
    * Parse the parameter file which will have information such as customer id, dc, etc.
    * @param fileName
    * @throws IOException
    */
   public void parseParamFile(String fileName) throws IOException
   {
      FileInputStream stream = new FileInputStream(new File(fileName));
      String params = "";
      String catType = "xml";

      try {
        FileChannel fc = stream.getChannel();
        MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

        params = Charset.defaultCharset().decode(bb).toString();
      }

      finally {
        stream.close();
      }

      //
      // email addresses are a comma separated list.
      if ( params != null && params.length() > 0 ) {
         String[] result = params.split("[,]");

         m_CustId = result[0];
         m_RptCust = result[1];
         m_LogicalId = result[2];
         catType = result[3];

         if ( result[4] != null )
            m_Zip = result[4].equalsIgnoreCase("y");

         //
         // Parse out any email addresses that will be added to the recipient list
         if ( result.length > 5 ) {
            for ( int i = 5; i < result.length; i++ )
               m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
         }
         
         //
         // Figure out which type of catalog to send.
         if ( catType.equalsIgnoreCase("txt") )
            m_CatType = CatalogType.txt;
         else {
            if ( catType.equalsIgnoreCase("xls") )
               m_CatType = CatalogType.xls;
            else
               m_CatType = CatalogType.xml;
         }
      }
   }
   
   /**
    * @param args
    */
   public static void main(String[] args) 
   {
      AubuchonCatExp rpt = new AubuchonCatExp();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[AubuchonCatExp] sending report request for %s", rpt.m_CustId));            
            rpt.sendRpt(new Object[] {rptXml});
            log.info(String.format("[AubuchonCatExp] report request for %s sent", rpt.m_CustId));
         }

         catch( Exception ex ) {
            log.error("[AubuchonCatExp]", ex);
         }
      }
      else
         msg = "[AubuchonCatExp] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }

}
