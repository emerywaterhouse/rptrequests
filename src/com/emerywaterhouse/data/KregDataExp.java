/**
 * File: KregDateExp.java
 * Description: Program to start the Kreg Tool data export report.
 *
 * @author Jeff Fisher
 *
 * Create Date: 12/23/2010
 * Last Update: $Id: KregDataExp.java,v 1.2 2012/10/02 19:26:30 jfisher Exp $
 *
 * History:
 *    $Log: KregDataExp.java,v $
 *    Revision 1.2  2012/10/02 19:26:30  jfisher
 *    fixed a problem with a null pointer.
 *
 *    Revision 1.1  2012/09/20 19:06:32  jfisher
 *    Moved to the report request project from kreg data
 *
 *    Revision 1.2  2012/05/08 10:20:12  jfisher
 *    updated web service url
 *
 *    Revision 1.1  2010/12/28 09:27:45  jfisher
 *    Initial add
 *
 */
package com.emerywaterhouse.data;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;

public class KregDataExp extends RptReq
{
   private String m_StartDate;
   private String m_EndDate;
   private StringBuffer m_Recips;

   /**
    *
    */
   public KregDataExp()
   {
      super();
      m_Recips = new StringBuffer();
   }

   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();

      m_StartDate = null;
      m_EndDate = null;
      m_Recips = null;
   }

   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
         calcDates();
         params.append(String.format(RptReq.paramTag, "startdate", "String", m_StartDate));
         params.append(String.format(RptReq.paramTag, "enddate", "String", m_EndDate));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "yes", "yes",
               "Kreg Item Movement",
               "com.emerywaterhouse.rpt.export.KregItemMovement",
               params.toString(),
               m_Recips.toString(),
               ""
         );
      }

      catch ( Exception ex ) {
         log.error("[kreg]", ex);
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Calculates the start and end dates based on the current date.  Always
    * get the previous month's data.
    *
    * @param params A string array that has the start and end date parameters.  An array is
    *    used so we get the values back to the calling method.
    */
   public void calcDates()
   {
      int month;
      GregorianCalendar cal = new GregorianCalendar();
      String[] months = {
         "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
         "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
      };

      try {
         cal.roll(Calendar.MONTH, false);
         month = cal.get(Calendar.MONTH);

         if ( month == Calendar.DECEMBER )
            cal.roll(Calendar.YEAR, false);

         m_StartDate = String.format("01-%s-%d", months[month], cal.get(Calendar.YEAR));
         m_EndDate = String.format("%d-%s-%d",
            cal.getActualMaximum(Calendar.DAY_OF_MONTH), months[month], cal.get(Calendar.YEAR)
         );
      }

      finally {
         cal = null;
         months = null;
      }
   }

   /**
    *
    * @return the start date
    */
   public String getStartDate()
   {
      return m_StartDate;
   }

   /**
    *
    * @return the end date
    */
   public String getEndDate()
   {
      return m_EndDate;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String recips = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 2: {
            recips = args[1];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be env, recips");
         }
      }

      //
      // email addresses are a comma separated list.
      if ( recips != null && recips.length() > 0 ) {
         String[] result = recips.split("[,]");

         for ( int i = 0; i < result.length; i++ )
            m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
      }
   }

   /**
    * Launches the report request.
    *
    * @param args The command line parameters.
    * @throws Exception
    */
   public static void main(String[] args)
   {
      KregDataExp rpt = new KregDataExp();
      String rptXml = null;
      String msg = null;

      log.info("[kreg] starting export");
      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[kreg] sending report request for dates: %s - %s", rpt.getStartDate(), rpt.getEndDate()));
            rpt.sendRpt(new Object[] {rptXml});
            log.info("[kreg] report request for %s sent");
         }

         catch( Exception ex ) {
            log.error("[kreg] ", ex);
         }
      }
      else
         msg = "[kreg] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }
}
