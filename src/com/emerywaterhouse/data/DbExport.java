/**
 * File: DbExport.java
 * Description: Used in conjunction with a cron job to build a monthly EmeryLink data base file.
 *
 * @author Jeff Fisher
 *
 * Create Date: 03/09/2011
 * Last Update: $Id: DbExport.java,v 1.2 2013/07/16 12:08:25 jfisher Exp $
 *
 * History
 *    $Log: DbExport.java,v $
 *    Revision 1.2  2013/07/16 12:08:25  jfisher
 *    Minor fixes
 *
 *    Revision 1.1  2012/09/20 19:42:52  jfisher
 *    Moved to the report request project from emlink
 *
 *    Revision 1.1  2011/03/15 17:29:21  jfisher
 *    Initial add
 *
 */
package com.emerywaterhouse.data;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;


public class DbExport extends RptReq
{

   private static final String accessKey  = "dfc87f3309372eae7fa04ea4f324627ba0639ea2e127ece31987b98d";
   private static final String uid        = "mobile";
   private static final String pwd        = "DJfTzlVW";
   private static final String devId      = "0";
   private static final String dbFilePath = "data";
   private static final String url        = "ftp.emeryonline.com";
   private StringBuffer m_Recips;

   public DbExport()
   {
      super();
      m_Recips = new StringBuffer();
   }

   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_Recips = null;
   }

   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;
      String dbName = null;
      String s1 = null;
      String s2 = null;
      String fileLoc = String.format("%s:%s", url, dbFilePath);

      try {
         parseCommandLine(args);

         s1 = accessKey.substring(0, 8);
         s2 = String.valueOf(System.currentTimeMillis());
         s2 = s2.substring(s2.length()-5, s2.length());
         dbName = String.format("%s%s.db", s1, s2);

         params.append(String.format(RptReq.paramTag, "accesskey", "String", accessKey));
         params.append(String.format(RptReq.paramTag, "dbname", "String", dbName));
         params.append(String.format(RptReq.paramTag, "dbpath", "String", dbFilePath));
         params.append(String.format(RptReq.paramTag, "uid", "String", uid));
         params.append(String.format(RptReq.paramTag, "pwd", "String", pwd));
         params.append(String.format(RptReq.paramTag, "devid", "String", devId));
         params.append(String.format(RptReq.paramTag, "appid", "String", "0"));


         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "yes", "no",
               "Mobile Data",
               "com.emerywaterhouse.rpt.export.MobileDbBuilder",
               params.toString(),
               m_Recips.toString(),
               String.format(ftpTag, fileLoc, uid, pwd)
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String recips = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 2: {
            recips = args[1];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be env, recips");
         }
      }

      //
      // email addresses are a comma separated list.
      if ( recips != null && recips.length() > 0 ) {
         String[] result = recips.split("[,]");

         for ( int i = 0; i < result.length; i++ )
            m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
      }
   }
   /**
    * Launches the report request.
    *
    * @param args The command line parameters.
    * @throws Exception
    */
   public static void main(String[] args)
   {
      DbExport rpt = new DbExport();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info("[dbexport] sending report request");
            rpt.sendRpt(new Object[] {rptXml});
            log.info("[dbexport] report request for %s sent");
         }

         catch( Exception ex ) {
            log.error("[dbexport] ", ex);
         }
      }
      else
         msg = "[dbexpoert] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }
}
