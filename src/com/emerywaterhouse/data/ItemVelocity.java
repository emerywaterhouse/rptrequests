/**
 * File: ItemVelocity.java
 * Description: Generates the report request to create the Velocity Code report.  This will be run by a cron job
 *    or the scheduler if that ever gets finished.
 *
 * Note: The report request has to have some command line parameters.  The DC is required and it should be either
 *    a 01 or a 02 for either Portland or Pittston.  The begin and end dates are optional.
 *
 *    The parameters will come from the script file that runs this java application.
 *
 * @author Jeff Fisher
 *
 * Create Date: 11/02/2009
 * Last Update: $Id: ItemVelocity.java,v 1.5 2012/07/17 12:51:40 jfisher Exp $
 *
 * History:
 *    $Log: ItemVelocity.java,v $
 *    Revision 1.5  2012/07/17 12:51:40  jfisher
 *    Updated with RptReq class changes.
 *
 *    Revision 1.4  2009/12/26 10:11:48  jfisher
 *    modified the email address for production and changed default params.
 *
 *    Revision 1.3  2009/11/03 18:57:53  jfisher
 *    pre-production
 *
 *
 */
package com.emerywaterhouse.data;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;


public class ItemVelocity extends RptReq
{
   private String m_Dc;
   private String m_BegDate;
   private String m_EndDate;
   private StringBuffer m_Recips;

   /**
    * Default constructor.
    */
   public ItemVelocity()
   {
      super();
      m_Recips = new StringBuffer();
      m_Recips.append(String.format(RptReq.recipTag, "Purchasing", "jfisher@emeryonline.com"));
   }

   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_Dc = null;
      m_BegDate = null;
      m_EndDate = null;
      m_Recips = null;
   }

   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
         params.append(String.format(RptReq.paramTag, "dc", "String", m_Dc));

         if ( m_BegDate != null && m_BegDate.length() > 0 )
            params.append(String.format(RptReq.paramTag, "begdate", "String", m_BegDate));

         if ( m_EndDate != null && m_EndDate.length() > 0 )
            params.append(String.format(RptReq.paramTag, "enddate", "String", m_EndDate));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "Item Veloacity",
               "com.emerywaterhouse.rpt.spreadsheet.VelocityCode",
               params.toString(),
               m_Recips.toString(),
               ""
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 2: {
            m_Dc = args[1];
            break;
         }

         case 4: {
            m_Dc = args[1];
            m_BegDate = args[2];
            m_EndDate = args[3];
            break;
         }

         default: {
            System.out.println("using default parameters");
         }
      }
   }

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      ItemVelocity rpt = new ItemVelocity();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[itemvel] sending report request for %s", "admin"));
            rpt.sendRpt(new Object[] {rptXml});
            log.info(String.format("[itemvel] report request for %s sent", "admin"));
         }

         catch( Exception ex ) {
            log.error("[itemvel] ", ex);
         }
      }
      else
         msg = "[itemvel] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }
}