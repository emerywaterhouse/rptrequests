/**
 * File: SalesRepShipments.java
 * Description: Generates the report request to create the RepShipments report.  This will be run by a cron job every day.
 *    at 4:00am currently.
 *
 * Parameters:
 *    Required - The name of the TM as it appears in EIS.
 *    Optional - The reporting date.  Defaults to sysdate-1.
 *
 *    The parameters will come from the script file that runs this java application.
 *
 * @author Jeff Fisher
 *
 * Create Date: 05/27/2011
 * Last Update: $Id: RepShipments.java,v 1.6 2014/01/10 15:38:46 jfisher Exp $
 *
 * History:
 *    $Log: RepShipments.java,v $
 *    Revision 1.6  2014/01/10 15:38:46  jfisher
 *    Added load date parameter
 *
 *    Revision 1.5  2011/11/29 07:51:34  jfisher
 *    Added the ftp element to the template
 *
 *    Revision 1.4  2011/08/06 06:10:38  jfisher
 *    Template changes
 *
 *    Revision 1.3  2011/07/28 03:52:51  jfisher
 *    modified the email parameter list handling.
 *
 *    Revision 1.2  2011/06/18 21:14:17  jfisher
 *    added terry hartford to the recip list
 *
 *    Revision 1.1  2011/06/04 14:38:39  jfisher
 *    Initial add
 *
 */
package com.emerywaterhouse.data;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;


public class RepShipments extends RptReq
{
   private String m_Rep;
   private String m_Date;
   private String m_LoadDate;
   private StringBuffer m_Recips;

   public RepShipments()
   {
      super();

      m_Recips = new StringBuffer();
   }

   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_Rep = null;
      m_Date = null;
      m_LoadDate = null;
      m_Recips = null;
   }

   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
         params.append(String.format(RptReq.paramTag, "Rep", "String", m_Rep));

         if ( m_Date != null && m_Date.length() > 0 )
            params.append(String.format(RptReq.paramTag, "RptDate", "String", m_Date));

         if ( m_LoadDate != null && m_LoadDate.length() > 0 )
            params.append(String.format(RptReq.paramTag, "LoadDate", "String", m_LoadDate));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "Rep Shipments",
               "com.emerywaterhouse.rpt.spreadsheet.RepShipments",
               params.toString(),
               m_Recips.toString(),
               ""
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;
      String recips = null;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 1: break;

         case 3: {
            m_Rep = args[1];
            recips = args[2];
            break;
         }

         case 4: {
            m_Rep = args[1];
            m_Date = args[2];
            recips = args[3];

            break;
         }

         case 5: {
            m_Rep = args[1];
            m_Date = args[2];
            m_LoadDate = args[3];
            recips = args[4];

            break;
         }
         default: {
            throw new Exception("incorrect number of parameters; should be env, rep, emailaddr or env, rep, report date, load date, emailaddr");
         }
      }

      //
      // email addresses are a comma separated list.
      if ( recips != null && recips.length() > 0 ) {
         String[] result = recips.split("[,]");

         for ( int i = 0; i < result.length; i++ )
            m_Recips.append(String.format(RptReq.recipTag, "", result[i].trim()));
      }
   }

   /**
    * Launches the report request.
    *
    * @param args The command line parameters.
    * @throws Exception
    */
   public static void main(String[] args)
   {
      RepShipments rpt = new RepShipments();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[repshipment] sending report request for %s", rpt.m_Rep));
            rpt.sendRpt(new Object[] {rptXml});
            log.info(String.format("[repshipment] report request for %s sent", rpt.m_Rep));
         }

         catch( Exception ex ) {
            log.error("[repshipment] ", ex);
         }
      }
      else
         msg = "[repshipment] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }

}
