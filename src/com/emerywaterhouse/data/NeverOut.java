/**
 * File: NeverOut.java
 * Description: Generates the report request to create Day End's Never Out report.
 *
 * Note: The report request has no command-line parameters. This request has been created for manually
 *       generating the Never Out report, for instances on which the automated report fails after Day End processing.
 *
 * @author Stephen Martel
 *
 * Create Date: 08/08/2016
 *
 * History:
 * 
 */

package com.emerywaterhouse.data;
import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;

public class NeverOut extends RptReq {

   private StringBuffer m_Recips;

   /**
    * Default constructor.
    */
   public NeverOut()
   {
      super();
      m_Recips = new StringBuffer();
      
      // TODO - hardcoded values from dayendjob.pas as of 8/8/16 - much easier to leave this in the code rather than passing them all in as command line args
      m_Recips.append(String.format(RptReq.recipTag, "Planning", "Planning@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Dean Frost", "dfrost@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Amanda Ireland", "aireland@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Lori Grenier", "lgrenier@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Mike Legere", "mlegere@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Joan Walsh", "jwalsh@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Camille Dubois", "cdubois@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Suzie Brockelbank", "sbrockelbank@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Colby Dufour", "cdufour@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Ben Braley", "bbraley@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Roberta Neal", "rneal@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Steve Frazier", "sfrazier@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Rebecca Darling", "rdarling@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Fred Arsenault", "FArsenault@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Keri Fitzgerald", "kfitzgerald@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Pam Emerson", "pemerson@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Patrick McGrady", "pmcgrady@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Terry Hartford", "THartford@emeryonline.com"));
      m_Recips.append(String.format(RptReq.recipTag, "Stephen Martel", "smartel@emeryonline.com"));
   }


   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_Recips = null;
   }

   
   /**
    * Builds the report request
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      StringBuffer params = new StringBuffer();
      String xml = null;

      try {
         parseCommandLine(args);
         
         // we must force a "dummy" param in, as the NeverOut report relies on code that runs in its setParams method
         params.append(String.format(RptReq.paramTag, "Dummy", "String", ""));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "Never Out",
               "com.emerywaterhouse.rpt.spreadsheet.NeverOut",
               params.toString(),
               m_Recips.toString(),
               ""
         );
      }
      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }
      finally {
         params = null;
      }

      return xml;
   }


   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {      
      // the first argument is the environment, "test" or "production".
      // all following args are recipient emails, which are treated as both the recipient name and email address
      
      int pcount = args.length;
      super.parseCommandLine(args);
      for (int x = 1; x < pcount; ++x) { // initialized after the environment param
         m_Recips.append(String.format(RptReq.recipTag, args[x], args[x]));
      }
   }


   /**
    * @param args
    */
   public static void main(String[] args)
   {
      NeverOut rpt = new NeverOut();
      String rptXml = null;
      String msg = null;

      rptXml = rpt.buildRptRequest(args);

      if ( rptXml != null && rptXml.length() > 0 ) {
         try {
            log.info(String.format("[neverout] sending report request for %s", "admin"));
            rpt.sendRpt(new Object[] {rptXml});
            log.info(String.format("[neverout] report request for %s sent", "admin"));
         }
         catch( Exception ex ) {
            log.error("[neverout] ", ex);
         }
      }
      else
         msg = "[neverout] error creating the report request bod";

      if ( rpt.getMsg().length() > 0 )
         log.error(rpt.getMsg());

      if ( msg != null && msg.length() > 0 )
         log.error(msg);
   }

}