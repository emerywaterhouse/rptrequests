/**
 * File: RptReq.java
 * Description: Template and base class for the report requests.  Contains the BOD string and some
 *    processing code for logging and setting up the environment.
 *  *
 * @author Jeff Fisher
 *
 * Create Date: 05/27/2011
 * Last Update: $Id: RptReq.java,v 1.8 2013/07/16 12:10:37 jfisher Exp $
 *
 * History:
 *    $Log: RptReq.java,v $
 *    Revision 1.8  2013/07/16 12:10:37  jfisher
 *    Added internal vars to enable setting params in the XML doc
 *
 *    Revision 1.7  2012/10/02 11:05:11  jfisher
 *    Fixed raw class warning on web service call
 *
 *    Revision 1.6  2012/09/20 19:07:23  jfisher
 *    Switched to Axis2 from wasp
 *
 *    Revision 1.5  2012/05/08 08:08:39  jfisher
 *    service address change
 *
 *    Revision 1.4  2011/11/29 07:50:58  jfisher
 *    Added the ftp element to the template
 *
 *    Revision 1.3  2011/08/06 06:10:19  jfisher
 *    variable zip and attachment changes
 *
 *    Revision 1.2  2011/06/09 15:36:45  jfisher
 *    added a try/catch at log initialization
 *
 *    Revision 1.1  2011/06/04 14:39:26  jfisher
 *    Initial add
 *
 */
package com.emerywaterhouse.template;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.AsyncAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public abstract class RptReq
{
   public static final String paramTag = "<Param pname=\"%s\" ptype=\"%s\" value=\"%s\"/>";
   public static final String recipTag = "<Recipient name=\"%s\" email=\"%s\"/>";
   public static final String ftpTag   = "<Ftp url=\"%s\" uid=\"%s\" pwd=\"%s\" />";

   public static final String procRptReqBod =
      "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
      "<ProcessReportRequest " +
      "xmlns=\"http://www.openapplications.org/oagis\" " +
      "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
      "xsi:schemaLocation=\"http://www.openapplications.org/oagis\" " +
      "revision=\"8.0\" environment=\"Production\" lang=\"en-US\">" +
      "<ApplicationArea>" +
      "<Sender>" +
      "<LogicalId>emery</LogicalId>" +
      "<Confirmation>Never</Confirmation>" +
      "<AuthorizationId>dfc87f3309372eae7fa04ea4f324627ba0639ea2e127ece31987b98d</AuthorizationId>" +
      "</Sender>" +
      "<CreationDateTime>%s</CreationDateTime>" +
      "<BODId>%s</BODId>" +
      "<UserArea xmlns:em=\"http://www.emeryonline.com/oagis\" xsi:schemaLocation=\"http://www.emeryonline.com/oagis http://www.emeryonline.com/oagis/Overlays/Emery/Resources/emery.xsd\">" +
      "<em:RoutingInfo></em:RoutingInfo>" +
      "</UserArea>" +
      "</ApplicationArea>" +
      "<DataArea>" +
      "<Process acknowledge=\"Always\" confirm=\"Always\"/>" +
      "<ReportRequest zipped=\"%s\" attachment=\"%s\">" +
      "   <ReportName>%s</ReportName>" +
      "   <ReportClass>%s</ReportClass>" +
      "   <UserId>admin</UserId>" +
      "   <Password>pwd</Password>" +
      "   <Params>%s</Params>" +
      "   <Recipients>%s</Recipients>" +
      "   %s" +
      "</ReportRequest>" +
      "</DataArea>" +
      "</ProcessReportRequest>";

   /** Log4J Logger */
   protected final static Logger log = Logger.getLogger("com.emerywaterhouse");

   /**
    * Environment enumeration
    */
   public static enum Environment {
      Test,
      Production
   };

   protected StringBuffer m_Msg;    /** internal message */
   protected Environment m_Env;     /** Environment variable used to determine where to send the report */
   protected boolean m_Confirm;
   protected boolean m_Acknowledge;
   protected String m_Sender;

   /**
    * Static initializer
    */
   {
      init();
   }

   /**
    * Default constructor.
    */
   public RptReq()
   {
      super();

      m_Msg = new StringBuffer();
      m_Env = Environment.Test;
      m_Confirm = true;
      m_Acknowledge = true;
      m_Sender = "cron";
   }

   /**
    * Overridden by descendant classes to build the report request XML bod
    * @param args The command line argument array from the main method.
    *
    * @return The BOD XML string.
    */
   protected abstract String buildRptRequest(String[] args);

   /**
    * Clears the contents of the message buffer.
    */
   public void clearMsg()
   {
      m_Msg.setLength(0);
   }

   /**
    * @return A string containing any messages accumulated during processing.
    */
   public String getMsg()
   {
      return m_Msg.toString();
   }

   /**
    * Initializes the application.  Gets called before construction in a static block
    */
   private void init()
   {
      try {
         DOMConfigurator.configure("logcfg.xml");
         AsyncAppender asyncAppender = (AsyncAppender)Logger.getRootLogger().getAppender("ASYNC");
         asyncAppender.setBufferSize(10);
         asyncAppender.setLocationInfo(true);
      }

      catch ( Exception ex ) {
         ex.printStackTrace();
      }
   }

   /**
    * Parses the command line.  Gets the first parameter which should always
    * be the environment.  Descendant classes should call this first and then
    * parse the rest of the parameters.
    *
    * @param args The command line parameter list.
    * @throws Exception
    */
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = 0;

      if ( args != null )
         pcount = args.length;

      //
      // The first argument is supposed to be the environment.
      if ( pcount > 0 ) {
         if ( args[0].equalsIgnoreCase("test") )
            m_Env = Environment.Test;
         else {
            if ( args[0].equalsIgnoreCase("production") )
               m_Env = Environment.Production;
            else
               throw new Exception("invalid environment parameter");
         }
      }
      else
         throw new Exception("missing command line parameters");
   }

   /**
    * Sends a report request to the report server.
    *
    * @param env The environment, either Test or Production
    * @param opParams The Object array that contains the web service call params for the report.
    * @throws Exception
    */
   @SuppressWarnings("rawtypes")
   public void sendRpt(Object[] opParams) throws Exception
   {
      String url = String.format("http://%s/axis2/services/RptRequest", m_Env == Environment.Production ? "service.emeryonline.com" : "localhost:8080");
      RPCServiceClient serviceClient = new RPCServiceClient();
      QName opMeth = new QName("http://websvc.emerywaterhouse.com", "requestReport");
      Class[] returnTypes = new Class[] { String.class };
      String result = "";

      try {
         serviceClient.getOptions().setTo(new EndpointReference(url));
         Object[] response = serviceClient.invokeBlocking(opMeth, opParams, returnTypes);
         result = (String)response[0];
         System.out.println(result);
      }

      finally {
         serviceClient = null;
         opMeth = null;
         returnTypes = null;
      }
   }
}
