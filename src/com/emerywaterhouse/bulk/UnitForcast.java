/**
 * File: UnitForcast.java
 * Description: Launches the Unit Forcast Excel spreadsheet report from a spreadsheet in a bulk report load.
 *    The parameters are currently fixed based on Cetta's input.
 *
 * @author Jeff Fisher
 *
 * Create Date: 08/11/2011
 * Last Update: $Id: UnitForcast.java,v 1.2 2011/11/29 07:53:56 jfisher Exp $
 * $Revision: 1.2 $
 *
 * History:
 *    $Log: UnitForcast.java,v $
 *    Revision 1.2  2011/11/29 07:53:56  jfisher
 *    Added the ftp element to the template
 *
 *    Revision 1.1  2011/08/13 04:05:48  jfisher
 *    initial add
 *
 */
package com.emerywaterhouse.bulk;

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.emerywaterhouse.oag.OagisUtils;
import com.emerywaterhouse.template.RptReq;

public class UnitForcast extends RptReq
{
   String m_Email;
   int m_VndId;
   String m_InFile;


   /**
    *
    */
   public UnitForcast()
   {
      super();
   }


   /**
    * Clean up any allocated resources.
    */
   @Override
   public void finalize() throws Throwable
   {
      super.finalize();
      m_Email = null;
      m_InFile = null;
   }

   /**
    * @see com.emerywaterhouse.template.RptReq#buildRptRequest(java.lang.String[])
    */
   @Override
   protected String buildRptRequest(String[] args)
   {
      String xml = null;
      StringBuffer params = new StringBuffer();

      try {
         params.append(String.format(RptReq.paramTag, "warehousename", "String", "PORTLAND"));
         params.append(String.format(RptReq.paramTag, "warehousefac", "String", "01"));
         params.append(String.format(RptReq.paramTag, "vendor", "String", String.valueOf(m_VndId)));
         params.append(String.format(RptReq.paramTag, "showdisc", "String", "false"));

         xml = String.format(RptReq.procRptReqBod,
               OagisUtils.getOagisDateTime(),
               OagisUtils.createBODId(),
               "no", "yes",
               "Unit Forcast",
               "com.emerywaterhouse.rpt.spreadsheet.UnitForecast",
               params.toString(),
               String.format(RptReq.recipTag, "", m_Email),
               ""
         );
      }

      catch ( Exception ex ) {
         m_Msg.append(ex.toString());
      }

      finally {
         params = null;
      }

      return xml;
   }

   /**
    * Check the command line for report request parameters.
    * @see RptReq.parseCommandLine
    */
   @Override
   public void parseCommandLine(String[] args) throws Exception
   {
      int pcount = args.length;

      super.parseCommandLine(args);

      switch ( pcount ) {
         case 1: break;

         case 2: {
            m_InFile = args[1];
            break;
         }

         default: {
            throw new Exception("incorrect number of parameters; should be environment and input_file");
         }
      }
   }

   /**
    *
    * @param email
    */
   public void setEmail(String email)
   {
      m_Email = email;
   }

   /**
    *
    * @param vndId
    */
   public void setVndId(int vndId)
   {
      m_VndId = vndId;
   }

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      UnitForcast rpt = new UnitForcast();
      String xml = null;
      String msg = null;
      POIFSFileSystem fs = null;
      HSSFWorkbook wb = null;
      HSSFSheet sheet = null;
      HSSFRow row = null;
      HSSFCell cell = null;
      int cellType = -1;
      int lastRow = 0;
      int curRow = 1;
      int procCount = 0;

      System.out.println("processing file");

      try {
         rpt.parseCommandLine(args);

         fs = new POIFSFileSystem(new FileInputStream(rpt.m_InFile));
         wb = new HSSFWorkbook(fs);
         sheet = wb.getSheetAt(1);

         lastRow = sheet.getLastRowNum();

         while ( curRow <= lastRow ) {
            row = sheet.getRow(curRow);

            if ( row != null ) {
               cell = row.getCell(0);

               if ( cell != null )
                  rpt.m_Email = cell.getRichStringCellValue().getString().trim();

               cell = row.getCell(2);

               if ( cell != null ) {
                  cellType = cell.getCellType();

                  if ( cellType == HSSFCell.CELL_TYPE_STRING )
                     rpt.m_VndId = Integer.parseInt(cell.getRichStringCellValue().getString().trim());
                  else
                     rpt.m_VndId = (int)cell.getNumericCellValue();
               }

               //
               // send the report to the report server based on the env param
               if ( rpt.m_Email != null && rpt.m_Email.length() > 0 && rpt.m_VndId > 0 ) {
                  xml = rpt.buildRptRequest(args);
                  procCount++;

                  if ( xml != null && xml.length() > 0 ) {
                     try {
                        log.info(String.format("[unitforcast] sending report request for %d", rpt.m_VndId));
                        rpt.sendRpt(new Object[] {xml});
                        log.info(String.format("[unitforcast] report request for %d sent", rpt.m_VndId));
                     }

                     catch( Exception ex ) {
                        log.error("[unitforcast] ", ex);
                     }
                  }
                  else
                     msg = "[unitforcast] error creating the report request bod";

                  if ( rpt.getMsg().length() > 0 )
                     log.error("[unitforcast] " + rpt.getMsg());

                  if ( msg != null && msg.length() > 0 )
                     log.error("[unitforcast] " + msg);
               }

               rpt.m_Email = "";
               rpt.m_VndId = 0;
               xml = "";
            }

            //
            // Allows us to send in blocks of 10 every 20 minutes.  Probably should be a command line
            // parameter.
            if ( procCount % 10 == 0 )
               Thread.sleep(1200000);

            curRow++;
         }

         System.out.println(String.format("processed %d report entries", procCount));
      }

      catch ( Exception ex ) {
         log.error("[unitforcast]", ex);
      }

      finally {
         fs = null;
         wb = null;
         sheet = null;
         row = null;
         cell = null;
      }
   }

}
