#!/bin/sh

RPTREQS=/usr/local/rptreqs
LIB=$RPTREQS/lib/*

#
# axis web service libs
AXIS2=/opt/axis2/lib/*

#
# local dir
CLASS_PATH=$CLASS_PATH:$RPTREQS:$LIB:$AXIS2

export CLASS_PATH
echo "running activant ecatalog weekly export"
java -cp $CLASS_PATH:com.emerywaterhouse.data.ECatalogExp.class -server com.emerywaterhouse.data.ECatalogExp production item ken.shawver@activant.com
